#! /bin/bash

# clear tmpfs
for node in `cat $MACHINES`
do
    ssh $node "rm /tmp/proc*"
done

source /cvmfs/cvmfs.grid.sinica.edu.tw/hpc/compiler/scripts/centos7_intel_2018.sh
# Compile program
cp /cvmfs/cvmfs.grid.sinica.edu.tw/twgrid/specfem3d_globe/specfem3d_globe . -r

cd specfem3d_globe

rm -rf DATA/Par_file
cp ../data/Par_file DATA/

make clean
make meshfem3D
make specfem3D

cd ..
cp -r specfem3d_globe/bin .

# Prepare DATA
mkdir DATA
cp data/STATIONS DATA/
cp data/CMTSOLUTION DATA/
cp data/Par_file DATA/

mkdir OUTPUT_FILES DATABASES_MPI
cp DATA/Par_file OUTPUT_FILES/
cp DATA/STATIONS OUTPUT_FILES/
cp DATA/CMTSOLUTION OUTPUT_FILES/

# script to run the mesher and the solver
# read DATA/Par_file to get information about the run
# compute total number of nodes needed
NPROC_XI=`grep ^NPROC_XI data/Par_file | cut -d = -f 2 `
NPROC_ETA=`grep ^NPROC_ETA data/Par_file | cut -d = -f 2`
NCHUNKS=`grep ^NCHUNKS data/Par_file | cut -d = -f 2 `

# total number of nodes is the product of the values read
numnodes=$(( $NCHUNKS * $NPROC_XI * $NPROC_ETA ))

##############################
echo "==========================="
echo "start running mesher `date`"

time mpirun -np $numnodes -machinefile $MACHINES bin/xmeshfem3D 2>&1 >> localout.txt


echo "MEMORY NEEDED"
size -d bin/xspecfem3D

echo "==========================="

echo "start running solver `date`"

echo $MACHINES
cat $MACHINES

time mpirun -np $numnodes -machinefile $MACHINES bin/xspecfem3D 2>&1 >> localout.txt

echo "==========================="

rm specfem3d_globe -rf
rm DATABASES_MPI -rf 

for node in `cat $MACHINES`
do
    ssh $node "rm /tmp/proc*"
done

exit 0
